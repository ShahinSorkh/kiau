//! This crate is a personal project for [me] to learn few things including Rust
//! and TDD.
//!
//! KIAU stands for [Karaj Islamic Azad University]. It is my university,
//! obviously, which I am going to make a helper for. This software is going to
//! be able to sign in to [my student portal] and extract my finance and
//! educational info and then help me manage and decide for my edu career along
//! the way. _I will make GUI in the far future._
//!
//! [me]: http://shahinsorkh.ir
//! [Karaj Islamic Azad University]: http://kiau.ac.ir
//! [my student portal]: http://edu.kiau.ac.ir

pub mod captcha;
pub mod http;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(4, 2 + 2);
    }
}
