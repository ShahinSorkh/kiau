//! Student portal http helper module
//!
//! # Example
//!
//! ```
//! let client = kiau::http::KiauClient::default().unwrap();
//!
//! //let resp = client.login("{{username}}", "{{password}}").unwrap();
//! //assert_eq!(200, resp.status());
//! //assert!(client.logged_in().unwrap());
//!
//! // client is now authenticated
//! // do whatever you are supposed to do
//!
//! // and then preferably logout at the end
//! //let resp = client.logout().unwrap();
//! //assert_eq!(200, resp.status());
//! //assert!(! client.logged_in().unwrap());
//! assert_eq!("has to work", "has to work");
//! ```

extern crate reqwest;

use std::time::Duration;

/// The main sturct to use for all http based actions.
pub struct KiauClient {
    captcha_url: String,
    login_url: String,
    default_url: String,
    client: reqwest::blocking::Client,
    mocking: bool
}

/// Construction methods:
impl KiauClient {
    fn with_base(base: String, mocking: bool) -> reqwest::Result<KiauClient> {
        Ok(KiauClient {
            captcha_url: format!("{}/{}", base, "Captcha.aspx"),
            login_url: format!("{}/{}", base, "login.aspx"),
            default_url: format!("{}/{}", base, "Default.aspx"),
            client: KiauClient::default_client()?,
            mocking,
        })
    }

    #[cfg(not(test))]
    /// Returns a default `KiauClient` object
    pub fn default() -> reqwest::Result<KiauClient> {
        KiauClient::with_base(String::from("http://edu.kiau.ac.ir"), false)
    }

    #[cfg(test)]
    /// Returns a default mocked `KiauClient` object
    pub fn default() -> reqwest::Result<KiauClient> {
        KiauClient::with_base(mockito::server_url(), true)
    }

    fn default_client() -> reqwest::Result<reqwest::blocking::Client> {
        reqwest::blocking::Client::builder()
            .user_agent("Mozilla/5.0 (Windows NT 10.0; rv:68.0) Gecko/20100101 Firefox/68.0")
            .cookie_store(true)
            .timeout(Duration::from_secs(30))
            .connect_timeout(Duration::from_secs(30))
            .build()
    }
}

impl KiauClient {
    fn get_login_page(&self) -> reqwest::Result<reqwest::blocking::Response> {
        self.client
            .get(self.login_url.as_str())
            .send()
    }

    fn post_login_page(&self, credentials: Vec<(&str, &str)>) -> reqwest::Result<reqwest::blocking::Response> {
        self.client
            .post(self.login_url.as_str())
            .form(&credentials)
            .send()
    }

    fn get_default_page(&self) -> reqwest::Result<reqwest::blocking::Response> {
        self.client
            .get(self.default_url.as_str())
            .send()
    }

    fn post_default_page(&self, credentials: Vec<(&str, &str)>) -> reqwest::Result<reqwest::blocking::Response> {
        self.client
            .post(self.default_url.as_str())
            .form(&credentials)
            .send()
    }
}

// added functionality
mod auth;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(4, 2 + 2);
    }
}
