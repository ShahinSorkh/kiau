use std::io;
use std::fs;
use std::error::Error;
use select::{document, predicate};
use crate::captcha;

/// Authentication methods:
impl super::KiauClient {
    fn get_captcha(&self) -> Result<String, Box<dyn Error>> {
        let mut _res = self.client.get(self.captcha_url.as_str()).send()?;

        if self.mocking {
            return Ok("1234".to_string());
        }

        let captcha_tmpname = String::from("/tmp/kiau-captcha.gif");
        let mut fp = fs::File::create(captcha_tmpname.as_str())?;
        io::copy(&mut _res, &mut fp)?;
        let solved_captcha = String::from(captcha::solve(captcha_tmpname.as_str())?);
        fs::remove_file(captcha_tmpname.as_str())?;

        Ok(solved_captcha)
    }

    /// Login the user using the given username and password
    pub fn login(&self, username: &str, password: &str) -> Result<reqwest::blocking::Response, Box<dyn Error>> {
        let res = self.get_login_page()?;
        let solved_captcha = self.get_captcha()?;

        let doc = document::Document::from_read(res)?;
        let hidden_inputs = doc.find(
            predicate::And(
                predicate::Name("input"),
                predicate::Attr("type", "hidden")
            )
        );

        let mut login_credentials: Vec<(&str, &str)> = vec![];
        for el in hidden_inputs.into_selection().iter() {
            login_credentials.push(
                (
                    el.attr("name").unwrap_or(""),
                    el.attr("value").unwrap_or("")
                )
            );
        }

        login_credentials.push(("txtUserName", username));
        login_credentials.push(("txtPassword", password));
        login_credentials.push(("texttasvir", solved_captcha.as_str()));
        login_credentials.push(("LoginButton0", "ورود+دانشجو"));

        Ok(self.post_login_page(login_credentials)?)
    }

    /// Check whether the client has an authenticated session yet
    pub fn logged_in(&self) -> reqwest::Result<bool> {
        let res = self.get_default_page()?;
        let text = res.text()?;

        Ok(text.contains("مدیر گروه"))
    }

    /// Logout the authenticated user
    pub fn logout(&self) -> Result<reqwest::blocking::Response, Box<dyn Error>>{
        let res = self.get_default_page()?;

        let doc = document::Document::from_read(res)?;
        let hidden_inputs = doc.find(
            predicate::And(
                predicate::Name("input"),
                predicate::Attr("type", "hidden")
            )
        );

        let mut logout_credentials: Vec<(&str, &str)> = vec![];
        for el in hidden_inputs.into_selection().iter() {
            logout_credentials.push(
                (
                    el.attr("name").unwrap_or(""),
                    el.attr("value").unwrap_or("")
                )
            );
        }
        logout_credentials.push(("ctl00$Button1", "خروج"));

        Ok(self.post_default_page(logout_credentials)?)
    }
}

#[cfg(test)]
mod tests {
    use mockito::{mock, Matcher};
    use crate::http;

    #[test]
    fn can_log_in() {
        let _m1 = mock("GET", "/login.aspx")
            .with_header("Set-Cookie", "ASP.NET_SessionId=qrvdtkahmapdircpyggxzqu3; path=/; HttpOnly")
            .with_body_from_file("assets/tests/http/login.aspx")
            .create();

        let _m2 = mock("GET", "/Captcha.aspx")
            .with_header("Content-Type", "image/gif")
            .create();

        let _m3 = mock("POST", "/login.aspx")
            .match_header("Content-Type", "application/x-www-form-urlencoded")
            .match_header("Cookie", "ASP.NET_SessionId=qrvdtkahmapdircpyggxzqu3")
            .match_body(
                Matcher::AllOf(
                    vec![
                        Matcher::UrlEncoded("__EVENTTARGET".to_string(), "evnttarg".to_string()),
                        Matcher::UrlEncoded("__EVENTARGUMENT".to_string(), "evntarg".to_string()),
                        Matcher::UrlEncoded("__VIEWSTATE".to_string(), "viwstat".to_string()),
                        Matcher::UrlEncoded("__VIEWSTATEGENERATOR".to_string(), "viwstatgen".to_string()),
                        Matcher::UrlEncoded("__EVENTVALIDATION".to_string(), "evntvalid".to_string()),
                        Matcher::UrlEncoded("txtUserName".to_string(), "123456789".to_string()),
                        Matcher::UrlEncoded("txtPassword".to_string(), "secret".to_string()),
                        Matcher::UrlEncoded("texttasvir".to_string(), "1234".to_string()),
                        Matcher::UrlEncoded("LoginButton0".to_string(), "ورود+دانشجو".to_string()),
                    ]
                )
            )
            .with_body_from_file("assets/tests/http/Default.aspx")
            .create();

        let _m4 = mock("GET", "/Default.aspx")
            .with_body_from_file("assets/tests/http/Default.aspx")
            .create();

        let client = http::KiauClient::default().unwrap();
        client.login("123456789", "secret").ok();
        assert_eq!(true, client.logged_in().unwrap());

        _m1.assert();
        _m2.assert();
        _m3.assert();
        _m4.assert();
    }

    #[test]
    fn can_log_out() {
        let _m1 = mock("GET", "/Default.aspx")
            .with_header("Set-Cookie", "ASP.NET_SessionId=qrvdtkahmapdircpyggxzqu3; path=/; HttpOnly")
            .with_body_from_file("assets/tests/http/Default.aspx")
            .create();

        let _m2 = mock("POST", "/Default.aspx")
            .match_header("Content-Type", "application/x-www-form-urlencoded")
            .match_header("Cookie", "ASP.NET_SessionId=qrvdtkahmapdircpyggxzqu3")
            .match_body(
                Matcher::AllOf(
                    vec![
                        Matcher::UrlEncoded("__EVENTTARGET".to_string(), "evnttarg".to_string()),
                        Matcher::UrlEncoded("__EVENTARGUMENT".to_string(), "evntarg".to_string()),
                        Matcher::UrlEncoded("__VIEWSTATE".to_string(), "viwstat".to_string()),
                        Matcher::UrlEncoded("__VIEWSTATEGENERATOR".to_string(), "viwstatgen".to_string()),
                        Matcher::UrlEncoded("__EVENTVALIDATION".to_string(), "evntvalid".to_string()),
                        Matcher::UrlEncoded("ctl00$Button1".to_string(), "خروج".to_string()),
                    ]
                )
            )
            .create();

        let _m3 = mock("GET", "/Default.aspx")
            .match_header("Cookie", "ASP.NET_SessionId=qrvdtkahmapdircpyggxzqu3")
            .with_body_from_file("assets/tests/http/login.aspx")
            .create();

        let client = http::KiauClient::default().unwrap();
        client.logout().unwrap();
        assert_eq!(false, client.logged_in().unwrap());

        _m1.assert();
        _m2.assert();
        _m3.assert();
    }
}
