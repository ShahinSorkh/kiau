//! Simple captcha resolver for sign in purposes.
//!
//! # Limits
//!
//! - This module works for [kiau captchas] only
//! - There is no `0`s in expected captchas, so if it returns `0` as any
//!   position, __this means an error__.
//!
//! [kiau captchas]: http://edu.kiau.ac.ir/Captcha.aspx
//!
//! # Example
//!
//! The test image is ![6729][captcha]
//!
//! [captcha]: https://gitlab.com/ShahinSorkh/kiau/raw/master/assets/tests/captchas/test.gif
//!
//! ```
//! let captcha = kiau::captcha::solve("assets/tests/captchas/test.gif").unwrap();
//!
//! assert_eq!("6729", captcha.as_str());
//! ```

extern crate image;

use std::collections::HashMap;

fn get_model() -> HashMap<u8, u8> {
    let mut model_builder: HashMap<u8, u8> = HashMap::new();
    model_builder.insert(117, 1);
    model_builder.insert(187, 2);
    model_builder.insert(167, 3);
    model_builder.insert(190, 4);
    model_builder.insert(198, 5);
    model_builder.insert(209, 6);
    model_builder.insert(142, 7);
    model_builder.insert(211, 8);
    model_builder.insert(205, 9);

    model_builder
}

fn extract_numbers(captcha: image::DynamicImage) -> [image::DynamicImage; 4] {
    [
        captcha.clone().crop(7, 6, 20, 26),
        captcha.clone().crop(37, 6, 20, 26),
        captcha.clone().crop(67, 8, 20, 26),
        captcha.clone().crop(97, 0, 20, 26),
    ]
}

fn count_whites(num: &image::DynamicImage) -> u8 {
    let mut count = 0;
    for pixel in num.to_luma().pixels() {
        if pixel[0] > 200 {
            count += 1;
        }
    }
    count
}

/// Returns solved captcha in an [`ImageResult`]`<`[`String`]`>` containing  4 digits.
///
/// [`ImageResult`]: https://docs.rs/image/0.22.3/image/type.ImageResult.html
/// [`String`]: https://doc.rust-lang.org/std/string/struct.String.html
///
/// # Example
///
/// The test image is ![6729][captcha]
///
/// [captcha]: https://gitlab.com/ShahinSorkh/kiau/raw/master/assets/tests/captchas/test.gif
///
/// ```
/// let captcha = kiau::captcha::solve("assets/tests/captchas/test.gif").unwrap();
///
/// assert_eq!("6729", captcha.as_str());
/// ```
///
/// # Undefined Behavior
///
/// If it could not infer the correct number seen in captcha, will replace it
/// with `0` and returns no error or panics or anything.
///
/// May it be safer to check for any possible `0`s in the returned `String`.
pub fn solve(filename: &str) -> image::ImageResult<String> {
    let model = get_model();
    let captcha = image::open(filename)?;
    let nums = extract_numbers(captcha);

    let mut res = String::new();
    for num in nums.iter() {
        let whites_count = count_whites(num);
        let num = model.get(&whites_count).unwrap_or_else(|| &0u8);
        res = format!("{}{}", res, num);
    }

    Ok(res)
}

#[cfg(test)]
mod tests {
    use crate::captcha;

    #[test]
    fn first_num() {
        assert_eq!(
            "1119",
            captcha::solve("./assets/tests/captchas/1xxx.gif").unwrap()
        );
        assert_eq!(
            "2496",
            captcha::solve("./assets/tests/captchas/2xxx.gif").unwrap()
        );
        assert_eq!(
            "3329",
            captcha::solve("./assets/tests/captchas/3xxx.gif").unwrap()
        );
        assert_eq!(
            "4988",
            captcha::solve("./assets/tests/captchas/4xxx.gif").unwrap()
        );
        assert_eq!(
            "5878",
            captcha::solve("./assets/tests/captchas/5xxx.gif").unwrap()
        );
        assert_eq!(
            "6272",
            captcha::solve("./assets/tests/captchas/6xxx.gif").unwrap()
        );
        assert_eq!(
            "7843",
            captcha::solve("./assets/tests/captchas/7xxx.gif").unwrap()
        );
        assert_eq!(
            "8397",
            captcha::solve("./assets/tests/captchas/8xxx.gif").unwrap()
        );
        assert_eq!(
            "9315",
            captcha::solve("./assets/tests/captchas/9xxx.gif").unwrap()
        );
    }

    #[test]
    fn second_num() {
        assert_eq!(
            "8128",
            captcha::solve("./assets/tests/captchas/x1xx.gif").unwrap()
        );
        assert_eq!(
            "1269",
            captcha::solve("./assets/tests/captchas/x2xx.gif").unwrap()
        );
        assert_eq!(
            "1352",
            captcha::solve("./assets/tests/captchas/x3xx.gif").unwrap()
        );
        assert_eq!(
            "1428",
            captcha::solve("./assets/tests/captchas/x4xx.gif").unwrap()
        );
        assert_eq!(
            "7549",
            captcha::solve("./assets/tests/captchas/x5xx.gif").unwrap()
        );
        assert_eq!(
            "3659",
            captcha::solve("./assets/tests/captchas/x6xx.gif").unwrap()
        );
        assert_eq!(
            "8769",
            captcha::solve("./assets/tests/captchas/x7xx.gif").unwrap()
        );
        assert_eq!(
            "6889",
            captcha::solve("./assets/tests/captchas/x8xx.gif").unwrap()
        );
        assert_eq!(
            "3992",
            captcha::solve("./assets/tests/captchas/x9xx.gif").unwrap()
        );
    }

    #[test]
    fn third_num() {
        assert_eq!(
            "4514",
            captcha::solve("./assets/tests/captchas/xx1x.gif").unwrap()
        );
        assert_eq!(
            "4525",
            captcha::solve("./assets/tests/captchas/xx2x.gif").unwrap()
        );
        assert_eq!(
            "5133",
            captcha::solve("./assets/tests/captchas/xx3x.gif").unwrap()
        );
        assert_eq!(
            "7843",
            captcha::solve("./assets/tests/captchas/xx4x.gif").unwrap()
        );
        assert_eq!(
            "6853",
            captcha::solve("./assets/tests/captchas/xx5x.gif").unwrap()
        );
        assert_eq!(
            "8463",
            captcha::solve("./assets/tests/captchas/xx6x.gif").unwrap()
        );
        assert_eq!(
            "7779",
            captcha::solve("./assets/tests/captchas/xx7x.gif").unwrap()
        );
        assert_eq!(
            "9687",
            captcha::solve("./assets/tests/captchas/xx8x.gif").unwrap()
        );
        assert_eq!(
            "7695",
            captcha::solve("./assets/tests/captchas/xx9x.gif").unwrap()
        );
    }

    #[test]
    fn forth_num() {
        assert_eq!(
            "9811",
            captcha::solve("./assets/tests/captchas/xxx1.gif").unwrap()
        );
        assert_eq!(
            "9592",
            captcha::solve("./assets/tests/captchas/xxx2.gif").unwrap()
        );
        assert_eq!(
            "1923",
            captcha::solve("./assets/tests/captchas/xxx3.gif").unwrap()
        );
        assert_eq!(
            "8194",
            captcha::solve("./assets/tests/captchas/xxx4.gif").unwrap()
        );
        assert_eq!(
            "2845",
            captcha::solve("./assets/tests/captchas/xxx5.gif").unwrap()
        );
        assert_eq!(
            "1766",
            captcha::solve("./assets/tests/captchas/xxx6.gif").unwrap()
        );
        assert_eq!(
            "4187",
            captcha::solve("./assets/tests/captchas/xxx7.gif").unwrap()
        );
        assert_eq!(
            "1438",
            captcha::solve("./assets/tests/captchas/xxx8.gif").unwrap()
        );
        assert_eq!(
            "6149",
            captcha::solve("./assets/tests/captchas/xxx9.gif").unwrap()
        );
    }
}
