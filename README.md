# KIAU

[![build](https://gitlab.com/ShahinSorkh/kiau/badges/master/pipeline.svg)](https://gitlab.com/ShahinSorkh/kiau)

This is a personal project to learn rust.

Any help on any topic is greatly appreciated.

Check [contribution guide](https://gitlab.com/ShahinSorkh/kiau/blob/master/CONTRIBUTING.md)
and [GPLv3 license](https://gitlab.com/ShahinSorkh/kiau/blob/master/LICENSE)
for contributing details.